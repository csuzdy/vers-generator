const SESSION_KEY = "SESSION_ID";
const USER_KEY = "USER_ID";

function guidGenerator() {
  var S4 = function () {
    return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
  };
  return (
    S4() +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    "-" +
    S4() +
    S4() +
    S4()
  );
}

function useSessionToken() {
  let sessionToken = sessionStorage.getItem(SESSION_KEY);
  if (sessionToken) {
    return sessionToken;
  } else {
    sessionToken = guidGenerator();
    sessionStorage.setItem(SESSION_KEY, sessionToken);
    return sessionToken;
  }
}

function useUserToken() {
  let userToken = localStorage.getItem(USER_KEY);
  if (userToken) {
    return userToken;
  } else {
    userToken = guidGenerator();
    localStorage.setItem(USER_KEY, userToken);
    return userToken;
  }
}

function useAnalytics() {
  const sessionToken = useSessionToken();
  const userToken = useUserToken();

  fetch("https://analytics.csuzdi.eu/event", {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": " application/json"
    },
    body: JSON.stringify({
      site: location.hostname,
      userToken,
      sessionToken,
      lang: navigator.language,
      size: `${window.innerWidth}x${window.innerHeight}`,
    }),
  });
}
