var maganhangzok = [
  "a",
  "á",
  "e",
  "é",
  "i",
  "í",
  "o",
  "ó",
  "ö",
  "ő",
  "u",
  "ú",
  "ü",
  "ű",
];

var stat = {};
Object.keys(data).forEach((k) => {
  stat[k] = data[k].map((line) => analyze(line));
});

function analyze(line) {
  const lowerCaseLine = line.toLowerCase();
  const maganhangzokSorban = lowerCaseLine
    .split("")
    .filter((a) => maganhangzok.includes(a));
  return {
    szotagszam: maganhangzokSorban.length,
    maganhangzok: maganhangzokSorban,
    utolsoszotag: wordTrim(
      lowerCaseLine.substr(
        lowerCaseLine.lastIndexOf(
          maganhangzokSorban[maganhangzokSorban.length - 1]
        )
      )
    ),
    sor: line,
    utolsoszo: wordTrim(
      lowerCaseLine.substr(lowerCaseLine.lastIndexOf(" ") + 1)
    ),
    sor: line,
  };
}

function rhyme(stat1, stat2) {
  if (Math.abs(stat1.szotagszam - stat2.szotagszam) > 1) {
    return false;
  }
  const lastIndex = stat1.szotagszam - 1;
  return (
    stat1.utolsoszo !== stat2.utolsoszo &&
    stat1.utolsoszotag === stat2.utolsoszotag &&
    stat1.maganhangzok.join("") !== stat2.maganhangzok.join("") &&
    stat1.maganhangzok[lastIndex - 1] === stat2.maganhangzok[lastIndex - 1] &&
    stat1.maganhangzok[lastIndex] === stat2.maganhangzok[lastIndex]
  );
}

function findRhymes(poet, line) {
  const lineStat = analyze(line);
  const res = stat[poet].filter((st) => rhyme(lineStat, st)).map((r) => r.sor);
  return [...new Set(res)];
}

function wordTrim(word) {
  return word?.replace(/[^a-záéíóöőúüű]+$/, "")?.replace("«", "");
}
