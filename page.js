class Poem {
  poem = [];

  addLine(line) {
    this.poem.push(line);
    document.getElementById("lineNum").innerText = this.poem.length + 1 + ".";
    document.getElementById("poem").innerHTML = this.poem
      .map((r) => `<h4 class="verse">${r}</h4>`)
      .join("");
  }
}

class Poets {
  selected = "Petőfi Sándor";

  constructor() {
    document.getElementById(this.selected).classList.add("selected");
    document.getElementById("poetForCount").innerText = this.selected;
    const all = document.querySelectorAll(".poet");

    all.forEach((e) =>
      e.addEventListener("click", (event) => {
        all.forEach((e) => e.classList.remove("selected"));
        event.target.classList.add("selected");
        this.selected = event.target.id;
        document.getElementById("poetForCount").innerText = this.selected;
        document.querySelector("#lineInput").dispatchEvent(new Event("keyup"));
      })
    );
  }
}

const emptyMatchedLine = `<h3 style="visibility: none">&nbsp;</h3><hr />`;

var poem = new Poem();
var poets;

function onLoad() {
  useAnalytics();
  poets = new Poets();
  const input = document.querySelector("#lineInput");
  document
    .getElementById("feedback")
    .setAttribute(
      "href",
      `mailto:${atob("Y3NhYmEuZGF2aWRAY3N1emRpLmh1")}?subject=vers.csuzdi.hu`
    );

  input.addEventListener("keyup", (event) =>
    firstLineChanged(event.target.value.trim())
  );
  firstLineChanged(input.value);

  registerServiceWorker();
}

function firstLineChanged(firstLine) {
  const result = document.querySelector("#matchingLines");
  if (firstLine.length > 5) {
    const arr = findRhymes(poets.selected, firstLine);
    document.getElementById("matchCount").innerText = arr.length;
    const htmlArr = arr
      .map(
        (r) =>
          `<h3 class="matchedLine" data-line="${r}">${r} <span class="addIcon ms-3">✅</span></h3><hr />`
      )
      .slice(0, 500);

    if (htmlArr.length < 5) {
      for (let i = htmlArr.length; i < 5; i++) {
        htmlArr.push(emptyMatchedLine);
      }
    }

    result.innerHTML = htmlArr.join("");

    document.querySelectorAll(".matchedLine").forEach((elem) =>
      elem.addEventListener("click", (event) => {
        if (event.target.classList.contains("matchedLine")) {
          addLineToPeom(event);
        }
      })
    );
  } else {
    result.innerHTML = [1, 2, 3, 4, 5]
      .map((r) => emptyMatchedLine)
      .slice(0, 5)
      .join("");
  }
}

function addLineToPeom(event) {
  const input = document.querySelector("#lineInput");
  poem.addLine(input.value);
  poem.addLine(event.target.dataset.line);
  input.value = "";
  input.dispatchEvent(new Event("keyup"));
}

function registerServiceWorker() {
  navigator.serviceWorker
    .register("/service-worker.js", {scope: "/", updateViaCache: "none"})
    .then((registration) => {
      console.info(
        "ServiceWorker registration successful with scope: ",
        registration.scope
      );
      return registration;
    })
    .catch((error) => {
      console.info("ServiceWorker registration failed: ", error);
    });
}
